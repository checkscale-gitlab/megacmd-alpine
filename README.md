# MEGAcmd for Alpine Linux

The people over at Mega haven't made a .apk file for Alpine, so I was left to figure out how to do this manually.  I share it here for anyone else who might need/want to use MEGAcmd tools in Alpine Linux.


## How to use it

Just pull it from Docker Hub

```bash
$ docker pull danielquinn/megacmd-alpine
```

## Why isn't it an auto-build?

Because Docker Hub doesn't support GitLab.  They should fix that.

